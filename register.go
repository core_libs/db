package core_db

import (
	"fmt"
	"log"

	"github.com/go-redis/redis/v8"
	"gitlab.com/core_libs/config"
	"gorm.io/gorm"
)

const (
	dbConfName = "db"
)

type dataBase struct {
	connections []connections

	dbConn    map[string]*gorm.DB
	redisConn map[string]*redis.Client
}

type connections struct {
	Connect string `json:"connect"`
	Name    string `json:"name"`
	Base    string `json:"base"`
}

var Bases *dataBase

func init() {
	Bases = &dataBase{}
	Bases.dbConn = make(map[string]*gorm.DB)
	Bases.redisConn = make(map[string]*redis.Client)

	Bases.register()
}

func (b *dataBase) register() {
	if !config.CheckOpenConfig(dbConfName) {
		log.Println("[core_db | WARNING] db_conn is missing")
		return
	}
	openCfg, err := config.GetOpenConfig[[]connections](dbConfName)
	if err != nil {
		panic(err)
	}
	for i := range openCfg {
		if !config.CheckHolderFormat(openCfg[i].Connect) {
			panic(fmt.Sprintf("[core_db] wrong secret format [%s]", openCfg[i].Connect))
		}

		secretVal, errSecret := config.SetSecret(openCfg[i].Connect, dbConfName)
		if errSecret != nil {
			panic(errSecret)
		}
		openCfg[i].Connect = secretVal
	}
	b.connections = openCfg
	b.connect()
}

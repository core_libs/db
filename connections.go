package core_db

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"strconv"
	"strings"

	"github.com/go-redis/redis/v8"
	_ "github.com/lib/pq"
	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func (b *dataBase) connect() {
	for _, c := range b.connections {
		switch c.Base {
		case "postgres":
			b.postgresql(c.Name, c.Connect)
		case "mysql":
			b.mysql(c.Name, c.Connect)
		case "redis":
			b.redis(c.Name, c.Connect)
		default:
			log.Fatalf("[core_db | ERROR] unknown data base [" + c.Base + "]")
		}
	}
}

func (b *dataBase) mysql(name, dsn string) {
	log.Printf("[core_db | ERROR] start to connect [%s]\n", name)

	gormConf := gorm.Config{}
	sqlDB, err := sql.Open("mysql", dsn)

	db, err := gorm.Open(mysql.New(mysql.Config{
		Conn: sqlDB,
	}), &gormConf)

	if err != nil {
		log.Fatalf("[core_db | ERROR] error to connect MySQL [%s] | Details: %s", name, err.Error())
	}

	log.Printf("[core_db] Connection [%s] success\n", name)
	b.dbConn[name] = db
}

func (b *dataBase) postgresql(name, dsn string) {
	log.Printf("[core_db] Start to connect [%s]\n", name)
	gormConf := gorm.Config{}
	open, err := sql.Open("postgres", dsn)
	if err != nil {
		log.Fatalf("[core_db | ERROR] error open connection [%s] | Details: %s", name, err.Error())
	}

	db, err := gorm.Open(postgres.New(postgres.Config{
		Conn: open,
	}), &gormConf)

	if err != nil {
		log.Fatalf("[core_db | ERROR] error to connect postgres [%s] | Details: %s", name, err.Error())
	}

	log.Printf("[core_db] Connection [%s] success\n", name)
	b.dbConn[name] = db
}

func (b *dataBase) redis(name, dsn string) {
	log.Printf("[core_db] Start to connect [%s]\n", name)
	redisOptions := redis.Options{}
	var host string
	var port string

	dsnData := strings.Split(dsn, " ")
	for _, dsnCfg := range dsnData {
		dsnCfg = strings.TrimSpace(dsnCfg)
		dsnCfgData := strings.Split(dsnCfg, "=")
		if len(dsnCfgData) != 2 {
			log.Fatalf("[core_db | ERROR] Incorrect configuration [%s] for [%s]", dsnCfg, name)
		}

		switch dsnCfgData[0] {
		case "host":
			host = dsnCfgData[1]
		case "port":
			port = dsnCfgData[1]
		case "password":
			redisOptions.Password = dsnCfgData[1]
		case "db":
			dbNum, err := strconv.Atoi(dsnCfgData[1])
			if err != nil {
				log.Fatalf("[core_db | ERROR] Incorrect db value [%s] for [%s]", dsnCfgData[1], name)
			}
			redisOptions.DB = dbNum
		default:
			log.Printf("[core_db | WARNING] unknown parameter [%s] for [%s]\n", dsnCfgData[0], name)
		}
	}

	if host == "" || port == "" {
		log.Fatalf("[core_db | ERROR] Incorrect db params. Host or Port are missing for [%s]", name)
	}

	redisOptions.Addr = fmt.Sprintf("%s:%s", host, port)
	rdb := redis.NewClient(&redisOptions)

	log.Printf("[core_db] Ping to [%s]\n", name)
	err := rdb.Ping(context.Background()).Err()
	if err != nil {
		log.Fatalf("[core_db | ERROR] Error to connect to [%s] | Details: %s", name, err.Error())
	}
	log.Printf("[core_db] Ping OK. Connection [%s] success\n", name)
	b.redisConn[name] = rdb
}

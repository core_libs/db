package core_db

import (
	"context"
	"errors"
	"fmt"
	"time"
)

func (b *dataBase) GetCache(ctx context.Context, name, store string) (string, error) {
	conn, ok := b.redisConn[name]
	if !ok {
		errMsg := fmt.Sprintf("[core_db] Connect with name [%s] not found\n", name)
		return "", errors.New(errMsg)
	}
	return conn.Get(ctx, store).Result()
}

func (b *dataBase) SetCache(ctx context.Context, name, store, val string, ttl time.Duration) error {
	conn, ok := b.redisConn[name]
	if !ok {
		errMsg := fmt.Sprintf("[core_db] Connect with name [%s] not found\n", name)
		return errors.New(errMsg)
	}

	return conn.Set(ctx, store, val, ttl).Err()
}

func (b *dataBase) DeleteCache(ctx context.Context, name, store string) error {
	conn, ok := b.redisConn[name]
	if !ok {
		errMsg := fmt.Sprintf("[core_db] Connect with name [%s] not found\n", name)
		return errors.New(errMsg)
	}

	return conn.Del(ctx, store).Err()
}

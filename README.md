# Core DB

!!! _Необходимо будет отказаться от `gorm`_

## Конфиги

Необходимо завести конфиги в `open` и `secret` группе с именами `db`

Пример `conf.json`:
```json
{
  "open": {
    "db": "db"
  },
  "secret": {
    "db": "db"
  }
}
```

Пример `open` конфига:
```json
[
  {
    "connect": "{{ CORE_PG }}",
    "name": "core",
    "base": "postgres"
  }
]
```
- `connect` - секрет из `.env` окружения
- `name` - имя, по которому будет вызываться коннект
- `base` - драйвер базы данных для подключения. Пока поддерживаются `postgres`, `mysql` и `redis`

Пример `secret` конфига:
```
CORE_PG: "host=localhost user=example password=secret dbname=postgres port=5432 binary_parameters=yes sslmode=disable connect_timeout=10"
```

## Использование

```go
res, err := data_base.Bases.Get(context.Background(), "core", "links.get_link", "link_hash")
```

В данном примере выполняется postgres функция `get_link` в схеме `links`

Первым параметром подается контекст. Вторым параметром ключ, который был записан в `open` конфиге под
параметром `name`. Третьим параметром передается полное название функции (_схема + имя функции_). И
остальными параметрами передаются переменные со значениями, которые должна обработать postgres функция
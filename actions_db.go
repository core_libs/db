package core_db

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"

	"gorm.io/gorm"
)

func (b *dataBase) CustomQuery(ctx context.Context, name, query string, params ...interface{}) (interface{}, error) {
	conn, ok := b.dbConn[name]
	if !ok {
		errMsg := fmt.Sprintf("[core_db] Connect with name [%s] not found\n", name)
		return nil, errors.New(errMsg)
	}

	var result []map[string]interface{}
	err := conn.WithContext(ctx).Raw(query, params...).Scan(&result).Error
	if err != nil {
		errMsg := fmt.Sprintf("[core_db] Query error [%s] | Details - %s\n", name, err)
		return nil, errors.New(errMsg)
	}
	return result, nil
}

func (b *dataBase) Get(ctx context.Context, name, procedureName string, params ...interface{}) (interface{}, error) {
	conn, ok := b.dbConn[name]
	if !ok {
		errMsg := fmt.Sprintf("[core_db] Connect with name [%s] not found\n", name)
		return nil, errors.New(errMsg)
	}

	query := b.createCaller("SELECT", procedureName, len(params))

	rqst := conn.Raw(query, params...)
	return b.getJSON(ctx, rqst)
}

func (b *dataBase) Call(ctx context.Context, name, procedureName string, params ...interface{}) (interface{}, error) {
	conn, ok := b.dbConn[name]
	if !ok {
		errMsg := fmt.Sprintf("[core_db] Connect with name [%s] not found\n", name)
		return nil, errors.New(errMsg)
	}

	query := b.createCaller("CALL", procedureName, len(params))
	rqst := conn.Raw(query, params...)
	return b.getJSON(ctx, rqst)
}

func (b *dataBase) createCaller(action, procedureName string, countParams int) string {
	query := action + " " + procedureName + "("
	if countParams > 0 {
		for i := 0; i < countParams; i++ {
			if i > 0 {
				query += ", "
			}
			query += fmt.Sprintf("$%d", i+1)
		}
	}
	query += ") as data"

	return query
}

func (b *dataBase) getJSON(ctx context.Context, request *gorm.DB) (interface{}, error) {
	res := struct {
		Data []byte `gorm:"column:data"`
	}{}

	var data interface{}

	err := request.WithContext(ctx).Scan(&res).Error
	if err != nil {
		return nil, err
	}

	if len(res.Data) == 0 {
		return nil, nil
	}

	err = json.Unmarshal(res.Data, &data)
	if err != nil {
		return nil, err
	}

	return data, nil
}
